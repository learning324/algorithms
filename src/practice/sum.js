sumNumbers = (numbers) => {
    let result = 0; // 1 execution
    for(let el of numbers) { // 1 execution initialization
        result += el; // 3 executions ( loop )
    }
    //     const result = numbers.reduce((sum, el) => sum + el, 0);
    // linear
    return result; // 1 execution
}

// T = 1 + 1 +  1 + n = 3 + n = 3 + 1*n
// T = 1*n
// T = n

console.log(sumNumbers([1, 2, 3, 4, 5]));